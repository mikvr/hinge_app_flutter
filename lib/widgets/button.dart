import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Button extends StatelessWidget {

  final String? text;
  final bool isValid;
  final Function()? onTap;

  const Button({
    Key? key,
    this.text,
    this.onTap,
    required this.isValid,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(20),
      onTap: onTap ?? () {},
      child: Ink(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          gradient: LinearGradient(
            colors: isValid ?
            [Color(0xff13DEA0), Color(0xff06B782)] :
            [Colors.blueGrey, Colors.grey],
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
          ),
        ),
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(left: 0, top: 40, right: 24, bottom: 20),
            ),
            SizedBox(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 20.w),
                      child: Text(
                        text!,
                        style: TextStyle(
                            fontSize: 18.w,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}