
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hinge/home_page.dart';
import 'package:hinge/main.dart';
import 'package:hinge/model/login.dart';
import 'package:hinge/services/login_service.dart';

import '../../core/dialog_core.dart';
import '../button.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginForm createState() => _LoginForm();
}


class _LoginForm extends State<LoginForm> {

  final pseudoController = TextEditingController();
  final pwdController = TextEditingController();
  final LoginService _service = LoginService();

  ValueNotifier<bool> isValid = ValueNotifier(false);

  @override
  void dispose() {
    pseudoController.dispose();
    pwdController.dispose();
    isValid.dispose();
    super.dispose();
  }

  /// Check if one of the field is empty
  bool isFieldEmpty() {
    return pseudoController.text.trim() == '' || pwdController.text.trim() == '';
  }

  /// Deactivate button if one field is empty
  void updateFormValidity(String value) {
    if (value == '' || isFieldEmpty()) {
      isValid.value = false;
    } else {
      isValid.value = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 0, top: 40, right: 24, bottom: 20),
          child: TextFormField(
            key: Key("_username"),
            onChanged: updateFormValidity,
            style: TextStyle(color: Colors.white),
            controller: pseudoController,
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.black38,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 1.5,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 2.0,
                ),
              ),
              border: OutlineInputBorder(),
              hintText: 'Pseudo',
              hintStyle: TextStyle(
                  color: Colors.white60
              ),
            ),
            keyboardType: TextInputType.text,
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 0, top: 0, right: 24, bottom: 20),
          child: TextFormField(
            key: Key("_password"),
            onChanged: updateFormValidity,
            controller: pwdController,
            obscureText: true,
            enableSuggestions: false,
            autocorrect: false,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.black38,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 1.5,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 2.0,
                ),
              ),
              border: OutlineInputBorder(),
              hintText: 'Mot de passe',
              hintStyle: TextStyle(
                  color: Colors.white60
              ),
            ),
            keyboardType: TextInputType.text,
          ),
        ),
        SizedBox(
          height: 250.h,
        ),
        ValueListenableBuilder(
          valueListenable: isValid,
          builder: (context, bool value, child) {
            return Container(
                padding: EdgeInsets.only(right: 24, bottom: 20),
                child: Button(
                    text: "Se connecter",
                    onTap: login,
                    isValid: value
                )
            );
          },
        ),
      ],
    );
  }


  void login() async {
    if (isValid.value) {
      final Login cmd = new Login(pseudoController.text, pwdController.text);
      final String? token = await _service.attemptLogin(cmd);
      print(token);
      if (token != null) {
        storage.write(key: "token", value: token);
        /*Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
          (Route<dynamic> route) => false
        );*/
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => HomePage())
        );
      } else {
        DialogCore.displayDialog(
          context,
          "Identification incorrecte",
          "Le pseudo et/ou le mot de passe sont incorrects"
        );
      }
    }
  }
}