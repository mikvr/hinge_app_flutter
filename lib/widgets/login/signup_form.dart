
import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

import '../../services/login_service.dart';
import '../button.dart';

class SignUpForm extends StatefulWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  _SignUpForm createState() => _SignUpForm();
}

class _SignUpForm extends State<SignUpForm> {

  final pseudoController = TextEditingController();
  final nameController = TextEditingController();
  final firstNameController = TextEditingController();
  final pwdController = TextEditingController();

  final format = DateFormat("dd/MM/yyyy");
  ValueNotifier<bool> isValid = ValueNotifier(false);

  final LoginService _service = LoginService();

  @override
  void dispose() {
    pseudoController.dispose();
    nameController.dispose();
    firstNameController.dispose();
    pwdController.dispose();
    isValid.dispose();
    super.dispose();
  }

  /// Check if one of the field is empty
  bool isFieldEmpty() {
    return pseudoController.text.trim() == '' ||
        nameController.text.trim() == '' ||
        firstNameController.text.trim() == '' ||
        pwdController.text.trim() == '';
  }

  /// Deactivate button if one field is empty
  void updateValidityState(String value) {
    if (value == '' || isFieldEmpty()) {
      isValid.value = false;
    } else {
      isValid.value = true;
    }
  }

  /// Deactivate button if date or other fields is empty
  void updateValidityDate(DateTime? value) {
    if (value != null || isFieldEmpty()) {
      isValid.value = false;
    } else {
      isValid.value = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 40, right: 24, bottom: 20),
          child: TextFormField(
            key: Key("_pseudo"),
            style: TextStyle(color: Colors.white),
            controller: pseudoController,
            onChanged: updateValidityState,
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.black38,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 1.5,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 2.0,
                ),
              ),
              border: OutlineInputBorder(),
              hintText: 'Pseudo',
              hintStyle: TextStyle(
                  color: Colors.white60
              ),
            ),
            keyboardType: TextInputType.text,
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 24, bottom: 20),
          child: TextFormField(
            key: Key("_name"),
            onChanged: updateValidityState,
            style: TextStyle(color: Colors.white),
            controller: nameController,
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.black38,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 1.5,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 2.0,
                ),
              ),
              border: OutlineInputBorder(),
              hintText: 'Nom',
              hintStyle: TextStyle(
                  color: Colors.white60
              ),
            ),
            keyboardType: TextInputType.text,
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 24, bottom: 20),
          child: TextFormField(
            key: Key("_firstname"),
            onChanged: updateValidityState,
            style: TextStyle(color: Colors.white),
            controller: firstNameController,
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.black38,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 1.5,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 2.0,
                ),
              ),
              border: OutlineInputBorder(),
              hintText: 'Prénom',
              hintStyle: TextStyle(
                  color: Colors.white60
              ),
            ),
            keyboardType: TextInputType.text,
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 24, bottom: 20),
          child: DateTimeField(
            style: TextStyle(color: Colors.white),
            onChanged: updateValidityDate,
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.black38,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 1.5,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 2.0,
                ),
              ),
              hintText: 'Date de naissance',
              hintStyle: TextStyle(
                  color: Colors.white60
              ),
            ),
            format: format,
            onShowPicker: (context, currentValue) {
              return showDatePicker(
                context: context,
                firstDate: DateTime(1900),
                initialDate: currentValue ?? DateTime(1998,9,29),
                lastDate: DateTime.now()
              );
            },
          ),
        ),
        Container(
          padding: EdgeInsets.only(right: 24, bottom: 20),
          child: TextFormField(
            key: Key("_password"),
            onChanged: updateValidityState,
            controller: pwdController,
            obscureText: true,
            enableSuggestions: false,
            autocorrect: false,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.black38,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 1.5,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20),
                borderSide: BorderSide(
                  color: Colors.black38,
                  width: 2.0,
                ),
              ),
              border: OutlineInputBorder(),
              hintText: 'Mot de passe',
              hintStyle: TextStyle(
                  color: Colors.white60
              ),
            ),
            keyboardType: TextInputType.text,
          ),
        ),
        SizedBox(
          height: 20.h,
        ),
        ValueListenableBuilder(
          valueListenable: isValid,
          builder: (context, bool value, child) {
            return Container(
              padding: EdgeInsets.only(right: 24, bottom: 20),
              child: Button(
                  text: "S'inscrire",
                  onTap: signup,
                  isValid: value
              )
            );
          },
        ),
      ],
    );
  }

  void signup() {
    // TODO
    if (isValid.value) {
      print("signup");
    }
  }
}