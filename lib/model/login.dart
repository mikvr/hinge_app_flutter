class Login {
  final String login;
  final String password;

  Login(this.login, this.password);

  factory Login.fromMap(Map<String, dynamic> json) {
    return Login(
      json['login'],
      json['password'],
    );
  }

  Map<String, dynamic> toJson() => {
    'login': login,
    'password': password,
  };
}