class Constant {
  // Localhost
  static const String API_URL = 'http://10.0.2.2:8081/api';
  static const String TOKEN = '';
  static Map<String, String> getHTTPHeaders(String? token) {
    if (token != null) {
      return {
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization': token
      };
    }
    return {
      'Content-type': 'application/json',
      'Accept': 'application/json'
    };
  }
}