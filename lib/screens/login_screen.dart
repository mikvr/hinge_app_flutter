import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hinge/widgets/login/signup_card.dart';

import '../widgets/login/login_card.dart';

class LoginScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff121421),
      body: SafeArea(
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Padding(
              padding: EdgeInsets.only(
                left: 28.w,
                right: 18.w,
                top: 36.h,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("Hinge",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 34.w,
                      fontWeight: FontWeight.bold)
                  )
                ]
              )
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 28.w,
                right: 18.w,
                top: 5.h,
              ),
              child: Container(
                height: 60.h,
                child: SizedBox(
                  width: 28.w,
                  child: Text("Connectez-vous pour continuer",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.w,
                      fontWeight: FontWeight.normal)
                  ),
                ),
              )
            ),
            SizedBox(
              height: 600.w,
              child: ListView(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                children: [
                  SizedBox(width: 28.w),
                  LoginCard(),
                  SizedBox(width: 20.w),
                  SignUpCard()
                ],
              ),
            ),
          ]
        ),
      ),
    );
  }

}