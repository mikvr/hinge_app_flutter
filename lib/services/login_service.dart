import 'dart:convert';

import 'package:http/http.dart' as http;

import '../core/constant.dart';
import '../model/login.dart';


class LoginService {

  /// Return the token.
  Future<String?> attemptLogin(Login credentials) async {
    final response = await http.post(
      Uri.parse(Constant.API_URL + '/login'),
      headers: Constant.getHTTPHeaders(null),
      body: jsonEncode(credentials.toJson())
    );

    if (response.statusCode == 204 && response.headers.containsKey("authorization"))
      return response.headers['authorization'];

    return null;
  }
}